import React from 'react';
import ReactDOM from 'react-dom';
import Plot from 'react-plotly.js';
import axios from 'axios';

class App extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
    };
  }

  componentDidMount() {
    let hasData = false;
    const result = {};
    console.log(process.env);
    const url = process.env.WEBPACK_SERVE
      ? "http://192.168.0.88:8086/query?db=sensors&q=select min(value) from temperature where time > '2018-03-10' group by time(3h)"
      : '/data';
    axios.get(url).then(response => {
      const data = response.data.results[0].series[0];
      const columns = {};
      data.columns.forEach((column, index) => {
        columns[index] = column;
        result[column] = [];
      });
      data.values.forEach(point => {
        point.forEach((value, index) => {
          result[columns[index]].push(value);
        });
      });
      this.setState({ data: result });
    });
  }

  render() {
    return (
      <Plot
        data={[
          {
            x: this.state.data['time'],
            y: this.state.data['min'],
            type: 'scatter',
            mode: 'lines+points',
          },
        ]}
        layout={{
          title: 'Vinkjellertemperatur',
          yaxis: { range: [0, 25] },
        }}
        config={{ displayModeBar: false }}
      />
    );
  }
}

ReactDOM.render(<App />, document.getElementById('container'));
